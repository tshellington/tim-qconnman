DEPTH = ../..
include($${DEPTH}/qconnman.pri)

TEMPLATE = app
TARGET = openconnect
DEPENDPATH += .
INCLUDEPATH += $${QCONNMAN_INCLUDEPATH}
LIBS += -L../../lib $${QCONNMAN_LIBS}
QT += network 

SOURCES += main.cpp
