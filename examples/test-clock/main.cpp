/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include "clock.h"

QString policyToString(Clock::UpdatePolicy policy)
{
    if (policy == Clock::AutoPolicy)
        return "auto";
    return "manual";
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Clock clock;
    qDebug() << "TimeUpdates = " << policyToString(clock.timeUpdates()) << endl
	     << "Timezone = " << clock.timezone() << endl
             << "Timeservers = " << clock.timeservers() << endl
	     << "TimezoneUpdates = " << policyToString(clock.timezoneUpdates()) << endl
	     << "Time = " << clock.time();
}
