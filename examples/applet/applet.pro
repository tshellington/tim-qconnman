DEPTH = ../..
include($${DEPTH}/qconnman.pri)

TEMPLATE = app
TARGET = applet
DEPENDPATH += .
INCLUDEPATH += $${QCONNMAN_INCLUDEPATH}
LIBS += -L../../lib $${QCONNMAN_LIBS}

contains(QT_VERSION, ^5\\..*) {
    QT += widgets
}

HEADERS += \
    ipv4widget.h \
    mainwindow.h \
    networklistwidget.h \
    networkitemwidget.h \
    servicewidget.h \
    spinnerwidget.h \
    technologyitemwidget.h \
    wiredpage.h \
    wirelesspage.h

SOURCES += \
    ipv4widget.cpp \
    main.cpp \
    mainwindow.cpp \
    networklistwidget.cpp \
    networkitemwidget.cpp \
    servicewidget.cpp \
    spinnerwidget.cpp \
    technologyitemwidget.cpp \
    wiredpage.cpp \
    wirelesspage.cpp

FORMS += \
    forms/ipv4widget.ui \
    forms/mainwindow.ui \
    forms/networkitemwidget.ui \
    forms/servicewidget.ui \
    forms/technologyitemwidget.ui \
    forms/wiredpage.ui \
    forms/wirelesspage.ui

RESOURCES += \
    applet.qrc
