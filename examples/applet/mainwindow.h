#ifndef _MAINWINDOW_H
#define _MAINWINDOW_H

#include <QHash>
#include <QWidget>
#include "ui_mainwindow.h"

class Manager;
class TechnologyPage;
class MainWindow: public QWidget,
                  private Ui::MainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private Q_SLOTS:
    void configureTechnology(const QModelIndex &technology);
    void createTechnologyItemWidgets(const QModelIndex &parent, int start, int end);

private:
    Manager *m_manager;
    QHash<QModelIndex, QWidget*> m_pages;

};

#endif
