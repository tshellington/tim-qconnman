#include <QPixmap>
#include <QDebug>

#include "technology.h"
#include "technologyitemwidget.h"

TechnologyItemWidget::TechnologyItemWidget(Technology *technology, QWidget *parent)
    : QWidget(parent),
      m_technology(technology)
{
    setupUi(this);
    connect(technology, SIGNAL(dataChanged()), this, SLOT(updateInformation()));
    updateInformation();
}

TechnologyItemWidget::~TechnologyItemWidget()
{
}

void TechnologyItemWidget::updateInformation()
{
    if (m_technology->isConnected())
        connectedLabel->setPixmap(QPixmap(":/images/connected.png"));
    else
        connectedLabel->setPixmap(QPixmap(":/images/disconnected.png"));

    label->setText(m_technology->name());

    QString technologyType = m_technology->type().toLower();
    if (technologyType == QLatin1String("ethernet"))
        icon->setPixmap(QPixmap(":/images/network-wired.png"));
    else if (technologyType == QLatin1String("wifi"))
        icon->setPixmap(QPixmap(":/images/network-wireless.png"));
    else if (technologyType == QLatin1String("vpn"))
        icon->setPixmap(QPixmap(":/images/protected.png"));
    else {
        qDebug() << "no icon for technology: " << technologyType;
        icon->setPixmap(QPixmap());
    }
}
