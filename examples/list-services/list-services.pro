DEPTH = ../..
include($${DEPTH}/qconnman.pri)

TEMPLATE = app
TARGET = list-services
DEPENDPATH += .
INCLUDEPATH += $${QCONNMAN_INCLUDEPATH}
LIBS += -L../../lib $${QCONNMAN_LIBS}

SOURCES += main.cpp
