/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

/*
 * This file was generated by qdbusxml2cpp version 0.7
 * Command line was: qdbusxml2cpp -c ServiceInterface -p serviceinterface -N dbus/connman-service.xml
 *
 * qdbusxml2cpp is Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#ifndef SERVICEINTERFACE_H_1343397545
#define SERVICEINTERFACE_H_1343397545

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>

/*
 * Proxy class for interface net.connman.Service
 */
class ServiceInterface: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    static inline const char *staticInterfaceName()
    { return "net.connman.Service"; }

public:
    ServiceInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = 0);
    ~ServiceInterface();

public Q_SLOTS: // METHODS
    inline QDBusPendingReply<> ClearProperty(const QString &name)
    {
        QList<QVariant> argumentList;
        argumentList << qVariantFromValue(name);
        return asyncCallWithArgumentList(QLatin1String("ClearProperty"), argumentList);
    }

    inline QDBusPendingReply<> Connect()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("Connect"), argumentList);
    }

    inline QDBusPendingReply<> Disconnect()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("Disconnect"), argumentList);
    }

    inline QDBusPendingReply<> MoveAfter(const QDBusObjectPath &service)
    {
        QList<QVariant> argumentList;
        argumentList << qVariantFromValue(service);
        return asyncCallWithArgumentList(QLatin1String("MoveAfter"), argumentList);
    }

    inline QDBusPendingReply<> MoveBefore(const QDBusObjectPath &service)
    {
        QList<QVariant> argumentList;
        argumentList << qVariantFromValue(service);
        return asyncCallWithArgumentList(QLatin1String("MoveBefore"), argumentList);
    }

    inline QDBusPendingReply<> Remove()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("Remove"), argumentList);
    }

    inline QDBusPendingReply<> ResetCounters()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("ResetCounters"), argumentList);
    }

    inline QDBusPendingReply<> SetProperty(const QString &name, const QDBusVariant &value)
    {
        QList<QVariant> argumentList;
        argumentList << qVariantFromValue(name) << qVariantFromValue(value);
        return asyncCallWithArgumentList(QLatin1String("SetProperty"), argumentList);
    }

Q_SIGNALS: // SIGNALS
    void PropertyChanged(const QString &name, const QDBusVariant &value);
};

#endif
