/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef CONNMANOBJECT_H
#define CONNMANOBJECT_H

#include <QObject>
#include <QDBusVariant>

class ConnManObject : public QObject
{
    Q_OBJECT
public:
    ConnManObject(QObject *parent = 0);
    ~ConnManObject();

public Q_SLOTS:
    virtual void propertyChanged(const QString &name, const QDBusVariant &value);
    void setObjectProperty(QObject *object, const QString &property, const QVariant &value);

};

#endif

