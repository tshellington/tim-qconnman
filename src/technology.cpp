/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include "qconnman_debug.h"
#include "technologyinterface.h"
#include "technology.h"

Technology::Technology(const QDBusObjectPath &path, const QVariantMap &properties, QObject *parent)
    : ConnManObject(parent),
      m_technologyInterface(0),
      m_path(path),
      m_powered(false),
      m_connected(false),
      m_tethering(false)
{
    m_technologyInterface =
        new TechnologyInterface("net.connman", path.path(), QDBusConnection::systemBus(), this);
    if (!m_technologyInterface->isValid()) {
        qConnmanDebug() << "unable to connect to service at path: " << path.path();
        return;
    }

    connect(m_technologyInterface, SIGNAL(PropertyChanged(QString,QDBusVariant)),
                             this, SLOT(propertyChanged(QString,QDBusVariant)));

    // set the initial properties
    foreach (QString property, properties.keys())
        propertyChanged(property, QDBusVariant(properties.value(property)));
}

Technology::~Technology()
{
}

QDBusObjectPath Technology::path() const
{
    return m_path;
}

bool Technology::isPowered() const
{
    return m_powered;
}

void Technology::setPowered(bool powered)
{
    QDBusPendingReply<> reply = m_technologyInterface->SetProperty("Powered", QDBusVariant(powered));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    }
}

bool Technology::isConnected() const
{
    return m_connected;
}

QString Technology::name() const
{
    return m_name;
}

QString Technology::type() const
{
    return m_type;
}

bool Technology::tetheringAllowed() const
{
    return m_tethering;
}

void Technology::setTetheringAllowed(bool allowed)
{
    QDBusPendingReply<> reply = m_technologyInterface->SetProperty("Tethering", QDBusVariant(allowed));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    }
}

QString Technology::tetheringIdentifier() const
{
    return m_tetheringIdentifier;
}

void Technology::setTetheringIdentifier(const QString &identifier)
{
    QDBusPendingReply<> reply =
        m_technologyInterface->SetProperty("TetheringIdentifier", QDBusVariant(identifier));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    }
}

QString Technology::tetheringPassphrase() const
{
    return m_tetheringPassphrase;
}

void Technology::setTetheringPassphrase(const QString &passphrase)
{
    QDBusPendingReply<> reply =
        m_technologyInterface->SetProperty("TetheringPassphrase", QDBusVariant(passphrase));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    }
}






void Technology::setPoweredInternal(bool powered)
{
    m_powered = powered;
    Q_EMIT poweredChanged(powered);
    Q_EMIT dataChanged();
}

void Technology::setConnectedInternal(bool connected)
{
    m_connected = connected;
    Q_EMIT dataChanged();
}

void Technology::setNameInternal(const QString &name)
{
    m_name = name;
    Q_EMIT dataChanged();
}

void Technology::setTypeInternal(const QString &type)
{
    m_type = type;
    Q_EMIT dataChanged();
}

void Technology::setTetheringAllowedInternal(bool allowed)
{
    m_tethering = allowed;
    Q_EMIT dataChanged();
}

void Technology::setTetheringIdentifierInternal(const QString &identifier)
{
    m_tetheringIdentifier = identifier;
    Q_EMIT dataChanged();
}

void Technology::setTetheringPassphraseInternal(const QString &passphrase)
{
    m_tetheringIdentifier = passphrase;
    Q_EMIT dataChanged();
}

