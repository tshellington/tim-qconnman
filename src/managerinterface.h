/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

/*
 * This file was generated by qdbusxml2cpp version 0.7
 * Command line was: qdbusxml2cpp -c ManagerInterface -p managerinterface -i types.h -N dbus/connman-manager.xml
 *
 * qdbusxml2cpp is Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#ifndef MANAGERINTERFACE_H_1350456402
#define MANAGERINTERFACE_H_1350456402

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>
#include "types.h"

/*
 * Proxy class for interface net.connman.Manager
 */
class ManagerInterface: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    static inline const char *staticInterfaceName()
    { return "net.connman.Manager"; }

public:
    ManagerInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = 0);

    ~ManagerInterface();

    Q_PROPERTY(QString State READ state WRITE setState)
    inline QString state() const
    { return qvariant_cast< QString >(property("State")); }
    inline void setState(const QString &value)
    { setProperty("State", QVariant::fromValue(value)); }

public Q_SLOTS: // METHODS
    inline QDBusPendingReply<QDBusObjectPath> ConnectProvider(const QVariantMap &provider)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(provider);
        return asyncCallWithArgumentList(QLatin1String("ConnectProvider"), argumentList);
    }

    inline QDBusPendingReply<QDBusObjectPath> CreateSession(const QVariantMap &settings, const QDBusObjectPath &notifier)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(settings) << QVariant::fromValue(notifier);
        return asyncCallWithArgumentList(QLatin1String("CreateSession"), argumentList);
    }

    inline QDBusPendingReply<> DestroySession(const QDBusObjectPath &session)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(session);
        return asyncCallWithArgumentList(QLatin1String("DestroySession"), argumentList);
    }

    inline QDBusPendingReply<QVariantMap> GetProperties()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("GetProperties"), argumentList);
    }

    inline QDBusPendingReply<QList<ObjectPropertyData> > GetServices()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("GetServices"), argumentList);
    }

    inline QDBusPendingReply<QList<ObjectPropertyData> > GetTechnologies()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("GetTechnologies"), argumentList);
    }

    inline QDBusPendingReply<> RegisterAgent(const QDBusObjectPath &path)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(path);
        return asyncCallWithArgumentList(QLatin1String("RegisterAgent"), argumentList);
    }

    inline QDBusPendingReply<> RegisterCounter(const QDBusObjectPath &path, int accuracy, int period)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(path) << QVariant::fromValue(accuracy) << QVariant::fromValue(period);
        return asyncCallWithArgumentList(QLatin1String("RegisterCounter"), argumentList);
    }

    inline QDBusPendingReply<> ReleasePrivateNetwork()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("ReleasePrivateNetwork"), argumentList);
    }

    inline QDBusPendingReply<> RemoveProvider(const QDBusObjectPath &provider)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(provider);
        return asyncCallWithArgumentList(QLatin1String("RemoveProvider"), argumentList);
    }

    inline QDBusPendingReply<QDBusObjectPath, QVariantMap, int> RequestPrivateNetwork(const QVariantMap &options)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(options);
        return asyncCallWithArgumentList(QLatin1String("RequestPrivateNetwork"), argumentList);
    }
    inline QDBusReply<QDBusObjectPath> RequestPrivateNetwork(const QVariantMap &options, QVariantMap &settings, int &fd)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(options);
        QDBusMessage reply = callWithArgumentList(QDBus::Block, QLatin1String("RequestPrivateNetwork"), argumentList);
        if (reply.type() == QDBusMessage::ReplyMessage && reply.arguments().count() == 3) {
            settings = qdbus_cast<QVariantMap>(reply.arguments().at(1));
            fd = qdbus_cast<int>(reply.arguments().at(2));
        }
        return reply;
    }

    inline QDBusPendingReply<> SetProperty(const QString &name, const QDBusVariant &value)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(name) << QVariant::fromValue(value);
        return asyncCallWithArgumentList(QLatin1String("SetProperty"), argumentList);
    }

    inline QDBusPendingReply<> UnregisterAgent(const QDBusObjectPath &path)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(path);
        return asyncCallWithArgumentList(QLatin1String("UnregisterAgent"), argumentList);
    }

    inline QDBusPendingReply<> UnregisterCounter(const QDBusObjectPath &path)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(path);
        return asyncCallWithArgumentList(QLatin1String("UnregisterCounter"), argumentList);
    }

Q_SIGNALS: // SIGNALS
    void PropertyChanged(const QString &name, const QDBusVariant &value);
    void ServicesChanged(const QList<ObjectPropertyData> &changedServices, const QList<QDBusObjectPath> &removedServices);
    void TechnologyAdded(const QDBusObjectPath &path, const QVariantMap &properties);
    void TechnologyRemoved(const QDBusObjectPath &path);
};

#endif
