/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef QCONNMAN_DEBUG_H
#define QCONNMAN_DEBUG_H

extern bool isDebugging;
#define qConnmanDebug           if (!::isDebugging); else qDebug
// #define qConnmanDebug       if (qgetenv("QCONNMAN_DEBUG").isEmpty()); else qDebug

#endif
