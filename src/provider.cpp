/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include "provider.h"

Provider::Provider()
{
    insert("Name", QString());
    insert("Host", QString());
    insert("VPN.Domain", QString());
}

QString Provider::name() const
{
    return value("Name").toString();
}

void Provider::setName(const QString &name)
{
    insert("Name", name);
}

QString Provider::host() const
{
    return value("Host").toString();
}

void Provider::setHost(const QString &host)
{
    insert("Host", host);
}

QString Provider::domain() const
{
    return value("VPN.Domain").toString();
}

void Provider::setDomain(const QString &domain)
{
    insert("VPN.Domain", domain);
}

int OpenConnectProvider::s_openconnectProviderMetaTypeId =
    qRegisterMetaType<OpenConnectProvider>("OpenConnectProvider");
OpenConnectProvider::OpenConnectProvider()
{
    insert("Type", "openconnect");
}

QString OpenConnectProvider::cookie() const
{
    return value("OpenConnect.Cookie").toString();
}

void OpenConnectProvider::setCookie(const QString &cookie)
{
    insert("OpenConnect.Cookie", cookie);
}

QString OpenConnectProvider::serverCert() const
{
    return value("OpenConnect.ServerCert").toString();
}

void OpenConnectProvider::setServerCert(const QString &serverCert)
{
    insert("OpenConnect.ServerCert", serverCert);
}


int OpenVpnProvider::s_openvpnProviderMetaTypeId = qRegisterMetaType<OpenVpnProvider>("OpenVpnProvider");
OpenVpnProvider::OpenVpnProvider()
{
    insert("Type", "openvpn");
}

QString OpenVpnProvider::caCert() const
{
    return value("OpenVPN.CACert").toString();
}

void OpenVpnProvider::setCaCert(const QString &caCert)
{
    insert("OpenVPN.CACert", caCert);
}

QString OpenVpnProvider::cert() const
{
    return value("OpenVPN.Cert").toString();
}

void OpenVpnProvider::setCert(const QString &cert)
{
    insert("OpenVPN.Cert", cert);
}

QString OpenVpnProvider::key() const
{
    return value("OpenVPN.Key").toString();
}

void OpenVpnProvider::setKey(const QString &key)
{
    insert("OpenVPN.Key", key);
}

int PptpProvider::s_pptpProviderMetaTypeId = qRegisterMetaType<PptpProvider>("PptpProvider");
PptpProvider::PptpProvider()
{
    insert("Type", "pptp");
}

QString PptpProvider::user() const
{
    return value("PPTP.User").toString();
}

void PptpProvider::setUser(const QString &user)
{
    insert("PPTP.User", user);
}

QString PptpProvider::password() const
{
    return value("PPTP.Password").toString();
}

void PptpProvider::setPassword(const QString &password)
{
    insert("PPTP.Password", password);
}

int L2tpProvider::s_l2tpProviderMetaTypeId = qRegisterMetaType<L2tpProvider>("L2tpProvider");
L2tpProvider::L2tpProvider()
{
    insert("Type", "l2tp");
}

QString L2tpProvider::user() const
{
    return value("L2TP.User").toString();
}

void L2tpProvider::setUser(const QString &user)
{
    insert("L2TP.User", user);
}

QString L2tpProvider::password() const
{
    return value("L2TP.Password").toString();
}

void L2tpProvider::setPassword(const QString &password)
{
    insert("L2TP.Password", password);
}
