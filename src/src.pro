include(../qconnman.pri)
TEMPLATE = lib
TARGET = qconnman
DESTDIR = ../lib
DEPENDPATH += .
INCLUDEPATH += .
CONFIG += qdbus $${QCONNMAN_LIBRARY_TYPE}
VERSION = $${QCONNMAN_VERSION}

# Input
HEADERS += connmanobject.h \
           manager.h \
           service.h \
           technology.h \
           agent.h \
           types.h \
           clock.h \
           provider.h \
           qconnman_debug.h
SOURCES += connmanobject.cpp \
           manager.cpp \
           service.cpp \
           technology.cpp \
           agent.cpp \
           types.cpp \
           clock.cpp \
           provider.cpp

# connman dbus interfaces
HEADERS += serviceinterface.h \
           managerinterface.h \
           technologyinterface.h \
           clockinterface.h \
           agentadaptor.h
SOURCES += serviceinterface.cpp \
           managerinterface.cpp \
           technologyinterface.cpp \
           clockinterface.cpp \
           agentadaptor.cpp

target.path = $${PREFIX}/$${LIBDIR}
header_files.files = $${HEADERS}
header_files.path = $${PREFIX}/include/qconnman
INSTALLS += target header_files

# pkg-config support
CONFIG += create_pc create_prl no_install_prl

QMAKE_PKGCONFIG_NAME = qconnman
QMAKE_PKGCONFIG_DESCRIPTION = QConnman provides a Qt wrapper around connman, the open source connection manager
QMAKE_PKGCONFIG_DESTDIR = pkgconfig
QMAKE_PKGCONFIG_LIBDIR = $$target.path
QMAKE_PKGCONFIG_INCDIR = $$header_files.path
equals(QCONNMAN_LIBRARY_TYPE, staticlib) {
    QMAKE_PKGCONFIG_CFLAGS = -DQCONNMAN_STATIC
} else {
    QMAKE_PKGCONFIG_CFLAGS = -DQCONNMAN_SHARED
}
unix:QMAKE_CLEAN += -r pkgconfig lib$${TARGET}.prl
