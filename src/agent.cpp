/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include "qconnman_debug.h"
#include "manager.h"
#include "agentadaptor.h"
#include "agent.h"

Agent::Agent(const QDBusObjectPath &path, Manager *parent)
    : QObject(parent),
      QDBusContext(),
      m_manager(parent),
      m_adaptor(0),
      m_path(path),
      m_inputRequest(NULL),
      m_browserRequest(NULL),
      m_errorRequest(NULL)
{
    m_adaptor = new AgentAdaptor(this);
    m_manager->registerAgent(this);
}

Agent::Agent(const QString &path, Manager *parent)
    : QObject(parent),
      m_manager(parent),
      m_adaptor(0),
      m_path(QDBusObjectPath(path)),
      m_inputRequest(NULL),
      m_browserRequest(NULL),
      m_errorRequest(NULL)
{
    m_adaptor = new AgentAdaptor(this);
    m_manager->registerAgent(this);
}

Agent::~Agent()
{
}

QDBusObjectPath Agent::path() const
{
    return m_path;
}

Manager *Agent::manager() const
{
    return m_manager;
}

void Agent::Release()
{
    qConnmanDebug() << Q_FUNC_INFO << "called";
}

void Agent::ReportError(const QDBusObjectPath &service, const QString &error)
{
    qConnmanDebug() << Q_FUNC_INFO << "for service: " << service.path();

    m_errorRequest = new ErrorRequest;
    m_errorRequest->service = service;
    m_errorRequest->error = error;

    Q_EMIT errorRaised();

    if (m_errorRequest->retry)
        sendErrorReply("net.connman.Agent.Error.Retry", "retry");
}

void Agent::RequestBrowser(const QDBusObjectPath &service, const QString &url)
{
    qConnmanDebug() << Q_FUNC_INFO << endl
             << "\tpath: " << service.path() << endl
             << "\t url: " << url;

    m_browserRequest = new BrowserRequest;
    m_browserRequest->service = service;
    m_browserRequest->url = url;

    Q_EMIT browserRequested();

    if (m_browserRequest->cancel)
        sendErrorReply("net.connman.Agent.Error.Canceled", "cancel");
}

QVariantMap Agent::RequestInput(const QDBusObjectPath &servicePath, const QVariantMap &fields)
{
    qConnmanDebug() << Q_FUNC_INFO << "for service: " << servicePath.path();
    if (!m_manager->hasService(servicePath)) {
        qConnmanDebug() << "\tinvalid service";
        return QVariantMap();
    }

    m_inputRequest = new InputRequest;
    m_inputRequest->service = servicePath;

    if (fields.keys().contains("SSID") || fields.keys().contains("Name"))
        Q_EMIT nameRequested();

    if (m_inputRequest->cancel)
    {
        sendErrorReply("net.connman.Agent.Error.Canceled", "");
        return fields;
    }

    if (fields.keys().contains("Passphrase"))
        Q_EMIT passphraseRequested();

    if (m_inputRequest->cancel)
    {
        sendErrorReply("net.connman.Agent.Error.Canceled", "");
        return fields;
    }

    QVariantMap response;

    if (fields.keys().contains("Passphrase"))
        response["Passphrase"] = m_inputRequest->response.passphrase;
    if (fields.keys().contains("SSID") || fields.keys().contains("Name"))
	{
        response["SSID"] = m_inputRequest->response.name;
        response["Name"] = m_inputRequest->response.name;
    }

    return response;
}
